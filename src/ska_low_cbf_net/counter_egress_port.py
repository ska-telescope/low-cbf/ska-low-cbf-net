import logging

from ska_low_cbf_net.connector_metrics import Metrics


class Counter_port_egress(Metrics):
    def __init__(self, target, gc, bfrt_info, ports):
        # Set up base class
        super(Counter_port_egress, self).__init__(target, gc, ports)

        self.table_counter_egress = bfrt_info.table_get("counter_egress_type")

    def _protocol(index: int) -> int:
        """Get the 4-bit protocol value (stored in bits 9-12) from an index"""
        return (index >> 9) & 0b1111

    def _port(index: int) -> int:
        """Get the 9-bit port value (stored in bits 0-8) from an index"""
        return index & 0b111111111

    def get_metric(self, metric):
        """Get counters associated to a given egress port.

        Returns:
            list of (dev port number+protocol, (packet_number, byte_number))
        """

        results = {}
        protocols = {0, 1, 2, 3, 4, 5, 6}
        ids = []
        for proto in protocols:
            ids.append(proto << 9 | metric)
        self.table_counter_egress.operations_execute(self.target, "Sync")
        entries_from_switch = self.table_counter_egress.entry_get(
            self.target,
            [
                self.table_counter_egress.make_key(
                    [self.gc.KeyTuple("$COUNTER_INDEX", i)]
                )
                for i in ids
            ],
        )
        for data, key in entries_from_switch:
            if key.to_dict()["$COUNTER_INDEX"]["value"] in ids:
                results[key.to_dict()["$COUNTER_INDEX"]["value"]] = (
                    data.to_dict()["$COUNTER_SPEC_PKTS"],
                    data.to_dict()["$COUNTER_SPEC_BYTES"],
                )

        # print(results)
        return results

    def get_metrics(self):
        """Get all counters associated to all active egress port.

        Returns:
            list of (dev port number+protocol, (packet_number, byte_number))
        """
        active_ports = self.ports.active_ports
        # print(active_ports)
        protocols = {0, 1, 2, 3, 4, 5, 6}
        ids = []
        for port in active_ports:
            for proto in protocols:
                ids.append(proto << 9 | port)
        entries = {}

        self.table_counter_egress.operations_execute(self.target, "Sync")
        entries_from_switch = self.table_counter_egress.entry_get(
            self.target,
            [
                self.table_counter_egress.make_key(
                    [self.gc.KeyTuple("$COUNTER_INDEX", i)]
                )
                for i in ids
            ],
        )
        for data, key in entries_from_switch:
            #   if key.to_dict()["$COUNTER_INDEX"]["value"] in ids:
            entries[key.to_dict()["$COUNTER_INDEX"]["value"]] = (
                data.to_dict()["$COUNTER_SPEC_PKTS"],
                data.to_dict()["$COUNTER_SPEC_BYTES"],
            )

        # print(entries)
        return entries

    def reset_metrics(self):
        """Reset counter"""

        # Reset indirect counters
        self.table_counter_egress.entry_del(self.target)
