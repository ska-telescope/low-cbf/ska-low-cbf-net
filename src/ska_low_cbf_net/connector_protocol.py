import logging


class Protocol(object):
    def __init__(self, target, gc):
        super(Protocol, self).__init__()
        # Set up base class
        self.log = logging.getLogger(__name__)
        # Keep a trace of the configured ports
        self.gc = gc
        self.target = target

    def _clear(self):
        """Remove all entries"""
        pass

    def add_default_entries(self):
        """Add broadcast and default entries"""
        pass

    def add_entry(self, match, action):
        """Add one entry.

        Keyword arguments:
            action -- action to act on a given match
            match -- match in the packet header
        """
        pass

    def add_entries(self, entry_list):
        """Add entries.

        Keyword arguments:
            entry_list -- a list of tuples: (match, action)
        """
        pass

    def remove_entry(self, match):
        """Remove one entry
        Keyword arguments:
           match -- match to remove from the table
        """

        pass

    def get_match(self, action):
        """Get match(es) associated to a action.
        Keyword arguments:
            action -- value of action
        Returns:
            (success flag, match or error message)
        """
        pass

    def get_action(self, match):
        """Get MAC addresses associated to a dev port
        Keyword arguments:
            match -- value of match in the table
        Returns:
            (success flag, action or error message)
        """
        pass

    def get_entries(self):
        """Get all forwarding entries.

        Returns:
            list of (match, action)
        """
        pass

    def update_entry(self, match, action):
        """
        Keyword arguments:
            action -- action to act on a given match
            match -- match in the packet header
        """
        pass
