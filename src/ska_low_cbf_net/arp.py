import binascii
import ipaddress
import logging
import re
import uuid

from ska_low_cbf_net.connector_protocol import Protocol


class Arp(Protocol):
    def __init__(self, target, gc, bfrt_info):
        # Set up base class
        super(Arp, self).__init__(target, gc)
        self.table = bfrt_info.table_get("arp_table")
        # self.multiplication = bfrt_info.table_get("multiplier_spead")

    def _clear(self):
        """Remove all entries"""
        entries_from_switch = self.table.entry_get(self.target)
        for data, key in entries_from_switch:
            self.table.entry_del(
                self.target,
                [
                    self.table.make_key(
                        [
                            self.gc.KeyTuple(
                                "target_ip",
                                key.to_dict()["target_ip"]["value"],
                            )
                        ]
                    )
                ],
            )

    def clear(self):
        self._clear()

    def add_default_entries(self):
        """Add broadcast and default entries"""

    def add_entry(self, match, action):
        """Add one entry.

        Keyword arguments:
            action -- Target IP
            match -- Mac Address
        """
        ip_address = int(ipaddress.IPv4Address((match)))
        mac_address = int.from_bytes(
            binascii.unhexlify(action.replace(":", "")),
            byteorder="big",
            signed=False,
        )
        self.table.entry_add(
            self.target,
            [self.table.make_key([self.gc.KeyTuple("target_ip", ip_address)])],
            [
                self.table.make_data(
                    [self.gc.DataTuple("my_eth_addr", mac_address)],
                    "answer_arp_request",
                )
            ],
        )

    def add_entries(self, entry_list):
        """Add entries.

        Keyword arguments:
            entry_list -- a list of tuples: (Target IP, Mac Address)
        """

        for (match, action) in entry_list:
            self.add_entry(match, action)

    def remove_entry(self, match):
        """Remove one entry"""
        ip_address = int(ipaddress.IPv4Address((match)))
        self.table.entry_del(
            self.target,
            [self.table.make_key([self.gc.KeyTuple("target_ip", ip_address)])],
        )

    def get_match(self, action):
        """Get frequency number(s) for a given egress port.

        Returns:
            (success flag, frequency number(s) or error message)
        """
        entries = []
        entries_from_switch = self.table.entry_get(self.target)
        for data, key in entries_from_switch:
            if data.to_dict()["dest_port"] is action:
                entries.append(
                    ipaddress.ip_address(
                        key.to_dict()["target_ip"]["value"]
                    ).__str__()
                )
        if len(entries) is 0:
            return (False, "Mac Address not found")

        return (True, entries)

    def get_action(self, match):
        """Get egress port for a given frequency number.

        Returns:
            (success flag, egress port or error message)
        """
        ip_address = int(ipaddress.IPv4Address((match)))
        results = self.table.entry_get(
            self.target,
            [
                self.table.make_key(
                    [
                        self.table.make_key(
                            [self.gc.KeyTuple("target_ip", ip_address)]
                        ),
                    ]
                )
            ],
        )

        return results

    def get_entries(self):
        """Get all forwarding entries.

        Returns:
            list of (frequency_no, dev port)
        """
        entries = {}
        entries_from_switch = self.table.entry_get(self.target)
        for data, key in entries_from_switch:
            if data.to_dict()["action_name"] == "answer_arp_request":
                entries[
                    ipaddress.ip_address(
                        key.to_dict()["target_ip"]["value"]
                    ).__str__()
                ] = ":".join(
                    re.findall("..", "%012x" % data.to_dict()["my_eth_addr"])
                )

        return entries

    def get_counters(self):
        """Get all forwarding entries.

        Returns:
            list of (match, counters (pkts, bytes))
        """
        entries = {}
        self.table.operations_execute(self.target, "SyncCounters")
        entries_from_switch = self.table.entry_get(self.target)
        for data, key in entries_from_switch:
            entries[
                ipaddress.ip_address(
                    key.to_dict()["target_ip"]["value"]
                ).__str__()
            ] = {
                "Pkts": data.to_dict()["$COUNTER_SPEC_PKTS"],
                "Bytes": data.to_dict()["$COUNTER_SPEC_BYTES"],
            }

        return entries

    def reset_counters(self):
        """Reset SPEAD counters"""

        # Reset direct counter
        self.table.operations_execute(self.target, "SyncCounters")
        resp = self.table.entry_get(self.target, flags={"from_hw": False})

        keys = []
        values = []

        for v, k in resp:
            keys.append(k)

            v = v.to_dict()
            k = k.to_dict()

            values.append(
                self.table.make_data(
                    [
                        self.gc.DataTuple("$COUNTER_SPEC_BYTES", 0),
                        self.gc.DataTuple("$COUNTER_SPEC_PKTS", 0),
                    ],
                    v["action_name"],
                )
            )

        self.table.entry_mod(self.target, keys, values)

    def update_entry(self, match, action):
        """
        Keyword arguments:
            action -- Dest port
            match -- Ingress port
        """
        ip_address = int(ipaddress.IPv4Address((match)))
        mac_address = int.from_bytes(
            binascii.unhexlify(action.replace(":", "")),
            byteorder="big",
            signed=False,
        )
        installed_action = False
        entries_from_switch = self.table.entry_get(self.target)
        for data, key in entries_from_switch:
            if key.to_dict()["target_ip"]["value"] == ip_address:
                installed_action = True
        if installed_action:
            self.table.entry_mod(
                self.target,
                [
                    self.table.make_key(
                        [self.gc.KeyTuple("target_ip", ip_address)]
                    )
                ],
                [
                    self.table.make_data(
                        [self.gc.DataTuple("my_eth_addr", mac_address)],
                        "answer_arp_request",
                    )
                ],
            )
            #   bfruntime_pb2.TableModIncFlag.MOD_INC_ADD)
        else:
            self.add_entry(match, action)
