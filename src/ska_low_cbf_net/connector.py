# -*- coding: utf-8 -*-
#
# This file is part of the ska_low_cbf_net project
#
# Copyright (c) 2021 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

import glob
import logging
import os
import re
import signal
import socket
import sys
from enum import IntEnum, unique

if "SDE_INSTALL" in os.environ and os.path.exists(os.environ["SDE_INSTALL"]):
    bfrt_location = "{}/lib/python*/site-packages/tofino".format(
        os.environ["SDE_INSTALL"]
    )
    sys.path.append(glob.glob(bfrt_location)[0])
    bfrt_loc = "{}/lib/python*/site-packages/tofino/bfrt_grpc".format(
        os.environ["SDE_INSTALL"]
    )
    sys.path.append(glob.glob(bfrt_loc)[0])
else:
    mockup_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), "mockup"
    )
    sys.path.append(mockup_path)

import bfrt_grpc.client as gc

from ska_low_cbf_net.arp import Arp
from ska_low_cbf_net.counter_egress_port import Counter_port_egress
from ska_low_cbf_net.counter_ingress_port import Counter_port_ingress

# from grpc_server import GRPCServer
# from ska_low_cbf_net.connector_cli import Cli
from ska_low_cbf_net.multiplier import Multiplier
from ska_low_cbf_net.ports import Ports
from ska_low_cbf_net.simple import Simple

mac_address_regex = re.compile(":".join(["[0-9a-fA-F]{2}"] * 6))
front_panel_regex = re.compile("([0-9]+)/([0-9]+)$")


def validate_ip(value):
    """Validate IP address string"""
    try:
        socket.inet_aton(value)
        return True
    except:
        return False


def _protocol(index: int) -> int:
    """Get the 4-bit protocol value (stored in bits 9-12) from an index"""
    return (index >> 9) & 0b1111


def _port(index: int) -> int:
    """Get the 9-bit port value (stored in bits 0-8) from an index"""
    return index & 0b111111111


class CommandError(Exception):
    """Command failed"""

    pass


class Connector:
    """Interface for p4 in-network processor"""

    @unique
    class Protocol(IntEnum):
        UNKNOWN = 0
        ARP = 1
        ICMP = 2
        IP_OTHER = 3
        UDP_OTHER = 4
        SPEAD = 5
        PSR = 6

    def critical_error(self, msg):
        self.log.critical(msg)
        logging.shutdown()
        raise RuntimeError(msg)

    def __init__(
        self, program: str, bfrt_ip: str, bfrt_port: str, client_id: int = 0
    ):
        """
        Create an interface to P4 switch
        :param program:
        :param bfrt_ip:
        :param bfrt_port:
        :return:
        """
        self.log = logging.getLogger(__name__)
        self.log.info("Perentie controller")
        self.protocol_names = {_.value: _.name for _ in self.Protocol}

        # Multicast group ID -> replication ID (= node ID) -> port
        # self.multicast_groups = {self.all_ports_mgid: {}}

        # create empty byte & packet tables
        self._i_bytes = {}
        self._i_packets = {}
        self._e_bytes = {}
        self._e_packets = {}
        self._arp_bytes = {}
        self._arp_packets = {}

        self.grpc_addr = bfrt_ip
        self.dev = 0  # Device 0
        # Target all pipes
        self.target = gc.Target(self.dev, pipe_id=0xFFFF)
        # Connect to BFRT server
        try:
            interface = gc.ClientInterface(
                "{}:{}".format(bfrt_ip, bfrt_port),
                client_id=client_id,
                device_id=self.dev,
            )
        except RuntimeError as runtime_error:
            msg = runtime_error.args[0] % runtime_error.args[1]
            self.critical_error(msg)
        else:
            self.log.info(
                "Connected to BFRT server {}:{}".format(bfrt_ip, bfrt_port)
            )

        try:
            interface.bind_pipeline_config(program)
        except gc.BfruntimeForwardingRpcException:
            self.critical_error("P4 program {} not found!".format(program))

        try:
            # Get all tables for program
            self.bfrt_info = interface.bfrt_info_get(program)

            # Ports table
            self.ports = Ports(self.target, gc, self.bfrt_info)
            # simple table
            self.simple = Simple(self.target, gc, self.bfrt_info)
            # arp manipulation
            self.arp = Arp(self.target, gc, self.bfrt_info)
            # multiplication of traffic
            self.multiplier = Multiplier(self.target, gc, self.bfrt_info)
            # Ingress counter
            self.counter_ingress_port = Counter_port_ingress(
                self.target, gc, self.bfrt_info, self.ports
            )
            # Egress counter
            self.counter_egress_port = Counter_port_egress(
                self.target, gc, self.bfrt_info, self.ports
            )

        except KeyboardInterrupt:
            self.critical_error("Stopping controller.")
        except Exception as e:
            self.log.exception(e)
            self.critical_error("Unexpected error. Stopping controller.")

    # Ports
    def load_ports(self, ports):
        """Load ports yaml file and enable front panel ports.

        Keyword arguments:
            ports -- list of port to configure

        Returns:
            (success flag, list of ports or error message)
        """
        print(ports)
        # Add ports
        success, error_msg = self.ports.add_ports(ports)
        if not success:
            return False, error_msg

        return True, ports

    def remove_ports(self, port_to_delete: str = None):
        """Remove all active ports

        Keyword arguments:
            ports -- list of port to configure

        Returns:
            (success flag, list of ports or error message)
        """
        if port_to_delete:
            re_match = front_panel_regex.match(port_to_delete.strip())
            if re_match and re_match.group(1):
                port = int(re_match.group(1))
                if not (1 <= port and port <= 65):
                    raise CommandError("Port number invalid")
            else:
                raise CommandError("Port number invalid")
            if re_match.group(2):
                lane = int(re_match.group(2))
                if lane not in range(0, 4):
                    raise CommandError("Invalid lane")
            else:
                lane = 0
            success, error_msg = self.ports.remove_port(port, lane)
        else:
            success, stats = self.get_port_statistics()
            if success:
                for stat in stats:
                    re_match = front_panel_regex.match(
                        stat["$PORT_NAME"].strip()
                    )
                    if re_match and re_match.group(1):
                        port = int(re_match.group(1))
                        if not (1 <= port and port <= 65):
                            raise CommandError("Port number invalid")
                    else:
                        raise CommandError("Port number invalid")
                    if re_match.group(2):
                        lane = int(re_match.group(2))
                        if lane not in range(0, 4):
                            raise CommandError("Invalid lane")
                    else:
                        lane = 0
                    success, error_msg = self.ports.remove_port(port, lane)

    def get_port_statistics(self, target_port: str = None):
        """Show active ports. If a front-panel/lane is provided,
        only that port will be shown.
        """
        port = None
        lane = None
        try:
            if target_port:
                re_match = front_panel_regex.match(target_port.strip())

                if re_match and re_match.group(1):
                    port = int(re_match.group(1))
                    if not (1 <= port and port <= 65):
                        raise CommandError("Port number invalid")
                else:
                    raise CommandError("Port number invalid")

                if re_match.group(2):
                    lane = int(re_match.group(2))
                    if lane not in range(0, 4):
                        raise CommandError("Invalid lane")
                else:
                    lane = 0

            success, stats = self.ports.get_stats(port, lane)
            if not success:

                raise CommandError(stats)
            return True, stats

        except CommandError as e:
            return False, e

    def get_counter_egress(self, egress_port: str = None):
        """Show number of packets per protocol counters associated to ports. If a front-panel/lane is provided,
        only that port will be shown.
        Otherwise it will show only active ports
        """
        entries = []
        try:
            if egress_port:
                re_match = front_panel_regex.match(egress_port.strip())
                if re_match and re_match.group(1):
                    port = int(re_match.group(1))
                    if not (1 <= port and port <= 65):
                        raise CommandError("Port number invalid")
                else:
                    raise CommandError("Port number invalid")
                if re_match.group(2):
                    lane = int(re_match.group(2))
                    if lane not in range(0, 4):
                        raise CommandError("Invalid lane")
                else:
                    lane = 0
                success, port_dev = self.ports.get_dev_port(port, lane)
                for ingress_port, ctr in self.counter_egress_port.get_metric(
                    port_dev
                ).items():
                    port = ingress_port & 0b111111111
                    success2, fp_port, fp_lane = self.ports.get_fp_port(port)
                    if not success2:
                        raise CommandError("Port number invalid")
                    else:
                        entry = {
                            "Port": "{}/{}".format(fp_port, fp_lane),
                            "Proto": "{}".format(
                                self.protocol_names[_protocol(ingress_port)]
                            ),
                            "Pkts": ctr[0],
                            "Bytes": ctr[1],
                        }
                        entries.append(entry)
            else:
                for (
                    ingress_port,
                    ctr,
                ) in self.counter_egress_port.get_metrics().items():
                    port = _port(ingress_port)
                    success2, fp_port, fp_lane = self.ports.get_fp_port(port)
                    if not success2:
                        raise CommandError("Port number invalid")
                    else:
                        entry = {
                            "Port": "{}/{}".format(fp_port, fp_lane),
                            "Proto": "{}".format(
                                self.protocol_names[_protocol(ingress_port)]
                            ),
                            "Pkts": ctr[0],
                            "Bytes": ctr[1],
                        }
                        entries.append(entry)
        except CommandError as e:
            return (False, e)
        return (True, entries)

    def get_counter_ingress(self, ingress_port: str = None):
        """Show number of packets per protocol counters associated to ports. If a front-panel/lane is provided,
        only that port will be shown.
        Otherwise it will show only active ports
        """
        entries = []
        try:
            if ingress_port:
                re_match = front_panel_regex.match(ingress_port.strip())
                if re_match and re_match.group(1):
                    port = int(re_match.group(1))
                    if not (1 <= port and port <= 65):
                        raise CommandError("Port number invalid")
                else:
                    raise CommandError("Port number invalid")
                if re_match.group(2):
                    lane = int(re_match.group(2))
                    if lane not in range(0, 4):
                        raise CommandError("Invalid lane")
                else:
                    lane = 0
                success, port_dev = self.ports.get_dev_port(port, lane)
                for ingress_port, ctr in self.counter_ingress_port.get_metric(
                    port_dev
                ).items():
                    port = _port(ingress_port)
                    success2, fp_port, fp_lane = self.ports.get_fp_port(port)
                    if not success2:
                        raise CommandError("Port number invalid")
                    else:
                        entry = {
                            "Port": "{}/{}".format(fp_port, fp_lane),
                            "Proto": "{}".format(
                                self.protocol_names[_protocol(ingress_port)]
                            ),
                            "Pkts": ctr[0],
                            "Bytes": ctr[1],
                        }
                        entries.append(entry)
            else:
                for (
                    ingress_port,
                    ctr,
                ) in self.counter_ingress_port.get_metrics().items():
                    port = _port(ingress_port)
                    success2, fp_port, fp_lane = self.ports.get_fp_port(port)
                    if not success2:
                        raise CommandError("Port number invalid")
                    else:
                        entry = {
                            "Port": "{}/{}".format(fp_port, fp_lane),
                            "Proto": "{}".format(
                                self.protocol_names[_protocol(ingress_port)]
                            ),
                            "Pkts": ctr[0],
                            "Bytes": ctr[1],
                        }
                        entries.append(entry)
        except CommandError as e:
            return (False, e)
        return (True, entries)

    def clear_multicast_group(self, session_id: int):
        """Remove multicast group and nodes for this session"""

        if session_id in self.multicast_groups:
            for node_id in self.multicast_groups[session_id]:
                self.pre.remove_multicast_node(node_id)
            self.pre.remove_multicast_group(session_id)

            del self.multicast_groups[session_id]

    def reset_ports_statistics(self):
        self.ports.reset_stats()

    def reset_counter_egress(self):
        self.counter_egress_port.reset_metrics()

    def reset_counter_ingress(self):
        self.counter_ingress_port.reset_metrics()

    # Simple Table
    def get_simple_table_entries(self):
        entries = []
        try:
            for ingress_port, dev_port in self.simple.get_entries().items():
                success, ing_fp_port, ing_fp_lane = self.ports.get_fp_port(
                    ingress_port
                )
                success2, fp_port, fp_lane = self.ports.get_fp_port(dev_port)
                if not success or not success2:
                    raise CommandError("Port number invalid")
                else:
                    entry = {
                        "ingress port": "{}/{}".format(
                            ing_fp_port, ing_fp_lane
                        ),
                        "port": "{}/{}".format(fp_port, fp_lane),
                    }
                    entries.append(entry)
            return True, entries
        except CommandError as e:
            return False, e

    def add_simple_table_entry(self, in_re_match, out_re_match):
        """Add Simple Table entry. The simple table match a given ingress port to an egress port.
        This function create a entry and take two arguments, first the ingress then egress port.
        """
        lane_in = None
        lane_out = None
        if in_re_match and in_re_match.group(1):
            port_in = int(in_re_match.group(1))
            if not (1 <= port_in and port_in <= 65):
                raise CommandError("Ingress Port number invalid")
        else:
            raise CommandError("Ingress Port number invalid")

        if in_re_match.group(2):
            lane_in = int(in_re_match.group(2))
            if lane_in not in range(0, 4):
                raise CommandError("Invalid Ingress lane")

        if out_re_match and out_re_match.group(1):
            port_out = int(out_re_match.group(1))
            if not (1 <= port_out and port_out <= 65):
                raise CommandError("Egress Port number invalid")
        else:
            raise CommandError("Egress Port number invalid")

        if out_re_match.group(2):
            lane_out = int(out_re_match.group(2))
            if lane_out not in range(0, 4):
                raise CommandError("Invalid Egress lane")
        success, e_port = self.ports.get_dev_port(port_out, lane_out)
        success, i_port = self.ports.get_dev_port(port_in, lane_in)
        self.simple.add_entry(i_port, e_port)

    def update_simple_table_entry(self, in_re_match, out_re_match):
        """
        Update Simple Table entry. The simple table match a given ingress port to an egress port.
        This function update/create a entry and take two arguments, first the ingress then egress port.
        """
        lane_in = None
        lane_out = None
        if in_re_match and in_re_match.group(1):
            port_in = int(in_re_match.group(1))
            if not (1 <= port_in and port_in <= 65):
                raise CommandError("Ingress Port number invalid")
        else:
            raise CommandError("Ingress Port number invalid")

        if in_re_match.group(2):
            lane_in = int(in_re_match.group(2))
            if lane_in not in range(0, 4):
                raise CommandError("Invalid Ingress lane")

        if out_re_match and out_re_match.group(1):
            port_out = int(out_re_match.group(1))
            if not (1 <= port_out and port_out <= 65):
                raise CommandError("Egress Port number invalid")
        else:
            raise CommandError("Egress Port number invalid")

        if out_re_match.group(2):
            lane_out = int(out_re_match.group(2))
            if lane_out not in range(0, 4):
                raise CommandError("Invalid Egress lane")
        success, e_port = self.ports.get_dev_port(port_out, lane_out)
        success, i_port = self.ports.get_dev_port(port_in, lane_in)
        self.simple.update_entry(i_port, e_port)

    def del_simple_table_entry(self, in_re_match):
        """Delete a Simple Table entry. The simple table match a given ingress port to an egress port.
        This function removes an entry and takes one argument, the ingress port.
        """
        lane_in = None
        lane_out = None
        if in_re_match and in_re_match.group(1):
            port_in = int(in_re_match.group(1))
            if not (1 <= port_in and port_in <= 65):
                raise CommandError("Ingress Port number invalid")
        else:
            raise CommandError("Ingress Port number invalid")

        if in_re_match.group(2):
            lane_in = int(in_re_match.group(2))
            if lane_in not in range(0, 4):
                raise CommandError("Invalid Ingress lane")

        success, i_port = self.ports.get_dev_port(port_in, lane_in)
        try:
            self.simple.remove_entry(i_port)
        except Exeception as e:
            raise e

    def clear_simple_table(self):
        self.simple.clear()

    # Multicast
    def create_muticast_session(self, session_id):
        """Create a multicast session  .

        Keyword arguments:
            session_id -- session id

        Returns:
            (success flag, list of ports or error message)
        """
        self.multiplier.create_multicast_session(session_id)

    def configure_multicast_session(self, id, rid, yid, brid, hash1, hash2):
        """Create a multicast session

        Keyword arguments:
            id -- session id
            rid -- rid
            yid -- yid
            brid -- brid
            hash1 -- hash1
            hash2 -- hash2
        Returns:
            (success flag, list of ports or error message)
        """
        return self.multiplier.configure_multicast_session(
            id, rid, yid, brid, hash1, hash2
        )

    def add_ingress_to_multicast_session(self, ingress, session):
        """Create a multicast session  .

        Keyword arguments:
            line -- ingress, session

        Returns:
            (success flag, list of ports or error message)
        """
        lane_in = None
        if ingress and ingress.group(1):
            port_in = int(ingress.group(1))
            if not (1 <= port_in and port_in <= 65):
                raise CommandError("Ingress Port number invalid")
        else:
            raise CommandError("Ingress Port number invalid")

        if ingress.group(2):
            lane_in = int(ingress.group(2))
            if lane_in not in range(0, 4):
                raise CommandError("Invalid Ingress lane")

        success, i_port = self.ports.get_dev_port(port_in, lane_in)
        print(i_port)

        return self.multiplier.associate_ingress_to_multicast(i_port, session)

    def remove_ingress_from_multicast_session(self, ingress: re.Match):
        """
        Remove an ingress port from the multicast session
        Keyword arguments:
            ingress -- port that needs to be removed from the multicast session
        Returns:
            (success flag, list of ports or error message)
        """
        lane_in = None
        if ingress and ingress.group(1):
            port_in = int(ingress.group(1))
            if not (1 <= port_in and port_in <= 65):
                raise CommandError("Ingress Port number invalid")
        else:
            raise CommandError("Ingress Port number invalid")

        if ingress.group(2):
            lane_in = int(ingress.group(2))
            if lane_in not in range(0, 4):
                raise CommandError("Invalid Ingress lane")

        success, i_port = self.ports.get_dev_port(port_in, lane_in)
        return self.multiplier.remove_ingress_from_multicast(i_port)

    def add_port_to_multicast_session(self, session, rid, port):
        """Create a multicast node and add it to a multicast group.

        Keyword arguments:
            session -- multicast group ID
            rid -- node ID
            port -- device port for the node

        Returns:
            (success flag, None or error message)
        """

        lane_in = None
        if port and port.group(1):
            port_in = int(port.group(1))
            if not (1 <= port_in <= 65):
                raise CommandError("Ingress Port number invalid")
        else:
            raise CommandError("Ingress Port number invalid")

        if port.group(2):
            lane_in = int(port.group(2))
            if lane_in not in range(0, 4):
                raise CommandError("Invalid Ingress lane")

        success, port_m = self.ports.get_dev_port(port_in, lane_in)
        rid_m = rid + port_m
        return self.multiplier.add_port_to_multicast_session(
            session, rid_m, port_m
        )

    def remove_port_from_multicast_session(self, session, rid, port):
        """Remove a multicast node and add it to a multicast group.

        Keyword arguments:
            session -- multicast group ID
            rid -- node ID
            port -- device port for the node

        Returns:
            (success flag, None or error message)
        """

        lane_in = None
        if port and port.group(1):
            port_in = int(port.group(1))
            if not (1 <= port_in <= 65):
                raise CommandError("Ingress Port number invalid")
        else:
            raise CommandError("Ingress Port number invalid")

        if port.group(2):
            lane_in = int(port.group(2))
            if lane_in not in range(0, 4):
                raise CommandError("Invalid Ingress lane")

        success, port_m = self.ports.get_dev_port(port_in, lane_in)
        rid_m = rid + port_m
        return self.multiplier.remove_port_from_multicast_session(
            session, rid_m
        )

    def add_ports_to_multicast_session(self, session, rid_ports):
        """add a list of port to a multicast session.
        Keyword arguments:
            session -- multicast group ID
            rid_ports -- list of tuples (node ID, device port)
        Returns:
            success indication
        """

        return self.multiplier.add_ports_to_multicast_session(
            session, rid_ports
        )

    def clear_multiplier(self):
        self.multiplier.clear()

    def get_multicast_configurations(self):
        """Return the list of multicast sessions and associated ports

        Return:
             list of multicast

        """
        # session_ports = self.multiplier.get_sessions_and_ports()
        session_port = self.multiplier.get_sessions_and_ports()
        session_ports_fp = {}
        for k in session_port:
            ports = []
            for port in session_port[k]:
                success2, fp_port, fp_lane = self.ports.get_fp_port(port)
                ports.append("{}/{}".format(fp_port, fp_lane))
            session_ports_fp[k] = ports

        return session_ports_fp

    # ARP
    def add_arp_table_entry(self, ip: str, mac: str):
        """Add ARP Table entry. The ARP table match an IP address to a Mac.
        This function create an entry and take two arguments, first the IP address then Mac.
        """

        self.arp.add_entry(ip, mac)

    def del_arp_table_entry(self, ip: str):
        """Delete one ARP Table entry. The ARP table match an IP address to a Mac.
        This function delete an entry and take one argument, the IP address.
        """

        self.arp.remove_entry(ip)

    def update_arp_table_entry(self, ip: str, mac: str):
        """Update ARP Table entry. The ARP table match an IP address to a Mac.
        This function create an entry and take two arguments, first the IP address then Mac.
        """

        self.arp.update_entry(ip, mac)

    def get_arp_table_counters(self):
        """Return counters of ARP table."""
        entries = []
        try:
            for match, counters in self.arp.get_counters().items():

                print(match)
                entry = {
                    "IP": "{}".format(match),
                    "Pkts": "{}".format(counters["Pkts"]),
                    "Bytes": "{}".format(counters["Bytes"]),
                }
                entries.append(entry)
            return True, entries
        except CommandError as e:
            return False, e

    def get_arp_table_entries(self):
        """Return entries of ARP table."""
        entries = []
        try:
            for match, dev_port in self.arp.get_entries().items():
                # success2, fp_port, fp_lane = self.ports.get_fp_port(dev_port)

                entry = {
                    "IP": "{}".format(match),
                    "Mac": "{}".format(dev_port),
                }
                entries.append(entry)
            return True, entries
        except CommandError as e:
            return False, e

    def reset_arp_counters(self):
        self.arp.reset_counters()

    def clear_arp_table(self):
        self.arp.clear()
