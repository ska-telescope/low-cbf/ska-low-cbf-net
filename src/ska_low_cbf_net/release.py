# -*- coding: utf-8 -*-
"""Release information for ska_low_cbf_net"""

name = """ska_low_cbf_net"""
version = "0.4.9"
version_info = version.split(".")
description = """SKA Low CBF Network - In-Network Processor common software"""
author = "CSIRO"
author_email = ""
license = """CSIRO Open Source Licence"""
url = """https://www.skatelescope.org/"""
copyright = """CSIRO"""
