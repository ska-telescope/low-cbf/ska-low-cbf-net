import pytest
from async_packet_test.context import TestContext

from ska_low_cbf_net.connector import Connector


@pytest.fixture(scope="module")
def context():
    ctx = TestContext()
    return ctx


@pytest.fixture(scope="module")
def ctrl():
    ctrl = Connector("tna_telemetry", "127.0.0.1", "50052", 0)
    yield ctrl
    # TODO check if these can be moved here?
    # ctrl.clear_simple_table()
    # ctrl.clear_spead_table()
