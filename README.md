Low CBF In-Network-Processor
============================
P4 Code, TANGO device servers etc for the SKA-low Correlator and Beam-Former's In-Network Processors.

## Documentation
[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-low-cbf-net/badge/?version=latest)](https://developer.skao.int/projects/ska-low-cbf-net/en/latest/?badge=latest)

The documentation for this project can be found in the `docs` folder, or browsed in the SKA development portal:

* [ska-low-cbf-net documentation](https://developer.skatelescope.org/projects/ska-low-cbf-net/en/latest/index.html "SKA Developer Portal: ska-low-cbf-net documentation")

## Project Avatar (Repository Icon)
[Cable icons created by Flat Icons - Flaticon](https://www.flaticon.com/free-icons/cable "cable icons")

# Changelog
### 0.4.9
* fixing update of basic table entry
### 0.4.8
* fix retrieval when entry does not exist
* new template for read the docs
### 0.4.7
* fixing arp display
### 0.4.6
* Removing multicast ports egress and ingress
* Removing basic table entries
* Removing ARP table entries
### 0.4.5
* fixing port reset statistics
### 0.4.4
* separation of the P4 code and python code
### 0.4.3
* Unit tests for numerous traffic table (ARP, basic, multiplier)
* Unit tests for port manipulation
* Unit tests for ingress and egress counters
### 0.4.2
* Fixing ARP table manipulation
### 0.4.1
* Connector has now a client ID for BFRT 
### 0.4.0
* Correlator table operational 
### 0.3.7
* Update of the port counters after reset
### 0.3.6
* Added reset of counters in the default CLI
### 0.3.5
* Change critical error behaviour to aid debugging (raise exception rather than
kill process with SIGTERM)
### 0.3.4
* Change package build configuration from setup.py to pyproject.toml
### 0.3.3
* Split PSR, SPEAD, and Tango code to
[ska-low-cbf-conn](https://gitlab.com/ska-telescope/ska-low-cbf-conn)

### 0.3.2
* Update LowCbfConnector Tango device to use new Connector class

### 0.3.1
* Inherit Makefiles from
[ska-cicd-makefile](https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile)
* Update CI includes
* LowCbfConnector runs again (not updated to use new Connector class / BFRuntime)
* Helm charts: changed app label to "ska-low-cbf" to assist integration testing

## 0.3.0
* Processor-code: loading switch with BFRuntime gRPC
* Connector: Using BFRuntime to perform:
 * Loading and retrieving of P4 binary
 * Table configuration and retrieval
 * Telemetry
 * Port configuration (see dependency)

### 0.2.3
* Processor-code: BFRuntime compilation flag

### 0.2.2
* Processor-code: Code consolidation

### 0.2.1
* Processor-code: Telemetry for egress port
* Container image & Python requirements: remove EngageSKA URLs

## 0.2.0
* LowCbfConnector: implement byteLossRate & packetLossRate (derived from cumulative SPEAD packet counters)
* Processor-code: Telemetry for ingress port and packet loss rate of SPEAD packets

## 0.1.0
* LowCbfConnector: add bytesLost & packetsLost counters

### 0.0.7
* Migrate to CAR


#Dependency

The Connector is currently relying on the ports.py file from the https://github.com/p4lang/p4app-switchML
project.
