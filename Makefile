# CAR_OCI_REGISTRY_HOST and PROJECT are combined to define
# the Docker tag for this project. The definition below inherits the standard
# value for CAR_OCI_REGISTRY_HOST = artefact.skao.int and overwrites
# PROJECT to give a final Docker tag
PROJECT = ska-low-cbf-net

# RELEASE_NAME is the release that all Kubernetes resources will be labelled
# with
RELEASE_NAME ?= test

# Fixed variables
# Timeout for gitlab-runner when run locally
TIMEOUT = 86400

CI_PROJECT_DIR ?= .

XAUTHORITY ?= $(HOME)/.Xauthority
THIS_HOST := $(shell ip a 2> /dev/null | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p' | head -n1)
DISPLAY ?= $(THIS_HOST):0

CI_PROJECT_PATH_SLUG ?= ska-low-cbf-net
CI_ENVIRONMENT_SLUG ?= ska-low-cbf-net

#
# include makefile to pick up the standard Make targets, e.g., 'make build'
# build, 'make push' docker push procedure, etc. The other Make targets
# ('make interactive', 'make test', etc.) are defined in this file.
#
include .make/release.mk
include .make/make.mk
include .make/python.mk
include .make/help.mk
include .make/docs.mk
include .make/oci.mk

CI_JOB_ID ?= local##pipeline job id

# define private overrides for above variables in here
-include PrivateRules.mak


# (This curl hack should disappear at some point...)
#PYTHON_VARS_BEFORE_PYTEST = mkdir -p ./src/ska_low_cbf_net && pwd \
# && curl https://raw.githubusercontent.com/p4lang/p4app-switchML/main/dev_root/controller/ports.py --output ./src/ska_low_cbf_net/ports.py --insecure\
# && PYTHONPATH=./src:./src:./src/ska_low_cbf_net KUBE_NAMESPACE=$(KUBE_NAMESPACE) HELM_RELEASE=$(RELEASE_NAME) TANGO_HOST=$(TANGO_HOST)

PYTHON_VARS_BEFORE_PYTEST = PYTHONPATH=./src:/app/src
#KUBE_NAMESPACE=$(KUBE_NAMESPACE) HELM_RELEASE=$(RELEASE_NAME) TANGO_HOST=$(TANGO_HOST)

PYTHON_VARS_AFTER_PYTEST = -m 'not (post_deployment or bfrt)' --forked \
						--disable-pytest-warnings

PYTHON_BUILD_TYPE = non_tag_setup

PYTHON_SWITCHES_FOR_FLAKE8=--ignore=F401,W503 --exit-zero

# override python.mk python-pre-test target
python-pre-test:
	@echo "python-pre-test: running with: $(PYTHON_VARS_BEFORE_PYTEST) $(PYTHON_RUNNER) pytest $(PYTHON_VARS_AFTER_PYTEST) \
	 --cov=src --cov-report=term-missing --cov-report xml:build/reports/code-coverage.xml --junitxml=build/reports/unit-tests.xml $(PYTHON_TEST_FILE)"

requirements: ## Install Dependencies
	poetry install

pipeline_unit_test: ## Run simulation mode unit tests in a docker container as in the gitlab pipeline
	@docker run --volume="$$(pwd):/home/tango/ska-low-cbf-net" \
		--env PYTHONPATH=src:src/ska_low_cbf_net --env FILE=$(FILE) -it $(ITANGO_DOCKER_IMAGE) \
		sh -c "cd /home/tango/ska-low-cbf-net && make requirements && make python-test"

.PHONY: pipeline_unit_test requirements
